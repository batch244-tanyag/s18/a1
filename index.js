console.log("hello World")
/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.
		-Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function

*/
	function addTwoNumbers(num1,num2){
		let sum = num1 + num2;
		console.log("Displayed sum of " + num1 + " and " + num2);
		console.log(sum);
	};
	addTwoNumbers(5, 15);



	function subtractTwoNumbers(num1,num2){
		let difference = num1 - num2;
		console.log("Displayed difference of " + num1 + " and " + num2);
		console.log(difference);
	};
	subtractTwoNumbers(20, 5);

/*

	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.
*/
	
	let num1= 50;
	let num2= 10;
	let product = num1 * num2;
	let quotient = num1 / num2;

	function multiplyTwoNumbers(num1,num2){
		console.log("The product of " + num1 + " and " + num2 + ":");
		console.log(product);
	};
	multiplyTwoNumbers(num1, num2);

	function divideTwoNumbers(num1,num2){
		console.log("The quotient of " + num1 + " and " + num2 + ":");
		console.log(quotient);
	};
	divideTwoNumbers(num1, num2);


/*

	3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.

*/
	const pie = 3.1416
	let radius = 15 
	let circleArea= pie * radius ** 2
	function findCircleArea(pie,radius){
	console.log("the result of getting the area of a circle with " + radius + " radius:");
	console.log(circleArea)
	}
	findCircleArea(pie, radius);

/*

	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
*/

	
	let num3 = 20;
	let num4 = 40;
	let num5 = 60;
	let num6 = 80;
	let averageVar = (num3 + num4 + num5 + num6)/4;

	function getAverage(num3, num4, num5, num6){
		console.log("the average of " + num3 + " " + num4 + " " + num5 + " and " + num6 + ":")
		console.log(averageVar);
	}
	getAverage(num3, num4, num5, num6);



/*

	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/




	let yourScore = 38
	let tScore=50
	let percentScore =yourScore/tScore
	let isPassingScore = percentScore >= 75/100

	function checkIfGradeIsPassed( yourScore, tScore){
	console.log("Is " + yourScore+"/"+tScore+ " a passing Score?");
	console.log(isPassingScore);
	}

	checkIfGradeIsPassed(yourScore,tScore);